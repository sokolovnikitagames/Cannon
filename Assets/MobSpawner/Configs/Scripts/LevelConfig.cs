using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New LevelConfig", menuName = "Level Config", order = 51)]
public class LevelConfig : ScriptableObject
{
    [SerializeField] private List<RoundConfig> _roundConfigs;

    public List<RoundConfig> RoundConfigs { get { return _roundConfigs; } }
}
