using UnityEngine;
using static MobFactoryBase;

[CreateAssetMenu(fileName = "New MobSpawnConfig", menuName = "Mob Spawn Config", order = 51)]
public class MobSpawnConfig : ScriptableObject
{
    [SerializeField] private float _startSpawnDelay;
    //[SerializeField] private float _count;
    [SerializeField] private float _spawnDelay;
    [SerializeField] private MobType _mobType;

    public float StartSpawnDelay { get { return _startSpawnDelay; } }
    public float SpawnDelay { get { return _spawnDelay; } }
    //public float Count { get { return _count; } }
    public MobType MobType { get { return _mobType; } }
}
