using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;
using Zenject;

public class MobSpawner : MonoBehaviour
{
    [SerializeField] private Vector2 _spawnOffset;
    [SerializeField] private MobFactoryBase _mobFactory;
    [SerializeField] private List<LevelConfig> _levelConfigs;

    private IOnCannonDiedEvent _cannonDiedEvent;

    private CancellationTokenSource _cancellationTokenSource;
    private int _actualLevelIndex;

    [Inject]
    private void Construct(IOnCannonDiedEvent cannonDiedEvent)
    {
        _cannonDiedEvent = cannonDiedEvent;
    }

    private void OnEnable()
    {
        _cannonDiedEvent.OnDieEvent += GameOver;
    }

    private void OnDisable()
    {
        _cannonDiedEvent.OnDieEvent -= GameOver;
    }

    private void Start()
    {
        _cancellationTokenSource = new CancellationTokenSource();
        _actualLevelIndex = 0;
        StartLevel(_actualLevelIndex, _cancellationTokenSource.Token);
    }
    
    private async void StartLevel(int index, CancellationToken token)
    {
        foreach (RoundConfig roundConfig in _levelConfigs[index].RoundConfigs) 
        {
            StartRound(roundConfig, token);
            await Task.Delay(TimeSpan.FromSeconds(roundConfig.Duration), token);
        }
    }
    
    private void StartRound(RoundConfig roundConfig, CancellationToken token)
    {
        foreach (MobSpawnConfig mobSpawnConfig in roundConfig.MobSpawnConfigs) 
        {
            SpawnMobConfig(mobSpawnConfig, roundConfig.Duration, token);
        }      
    }   

    private async void SpawnMobConfig(MobSpawnConfig mobSpawnConfig, float roundDuration, 
        CancellationToken token)
    {
        float time = 0;
        await Task.Delay(TimeSpan.FromSeconds(mobSpawnConfig.StartSpawnDelay), token);
        time += mobSpawnConfig.StartSpawnDelay;
        while (time <= roundDuration) 
        {
            Vector3 randomOffset = GetRandomOffset(_spawnOffset);
            _mobFactory.Spawn(mobSpawnConfig.MobType, transform.position + 
                new Vector3(randomOffset.x, transform.position.y, randomOffset.y), transform.rotation);
            await Task.Delay(TimeSpan.FromSeconds(mobSpawnConfig.SpawnDelay), token);
            time += mobSpawnConfig.SpawnDelay;
        }     
    }

    private Vector2 GetRandomOffset(Vector2 spawnOffset)
    {
        Vector2 randomOffset = new Vector2();
        randomOffset.x = UnityEngine.Random.Range(-1 * spawnOffset.x, spawnOffset.x);
        randomOffset.y = UnityEngine.Random.Range(-1 * spawnOffset.y, spawnOffset.y);
        return randomOffset;
    }

    private void GameOver()
    {
        _cancellationTokenSource.Cancel();
    }
}
