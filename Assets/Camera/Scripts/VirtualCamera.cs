using Cinemachine;
using UnityEngine;
using Zenject;

public class VirtualCamera : MonoBehaviour
{
    private CinemachineVirtualCamera _virtualCamera;
    private CannonBase _cannon;

    [Inject]
    private void Construct(CannonBase cannon)
    {
        _cannon = cannon;
    }

    private void Start()
    {
        _virtualCamera = GetComponent<CinemachineVirtualCamera>();
        _virtualCamera.Follow = _cannon.transform;
    }
}
