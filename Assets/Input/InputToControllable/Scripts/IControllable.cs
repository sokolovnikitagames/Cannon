using UnityEngine;

public interface IControllable
{
    public void Rotate(Vector2 rotateDirection);

    public void Attack();
}
