using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class InputToControllable : MonoBehaviour
{
    private IControllable _controllableObject;
    private InputActions _inputActions;
    private Vector2 _rotateInput;

    [Inject]
    private void Construct(IControllable controllableObject)
    {
        _controllableObject = controllableObject;
    }

    private void Awake()
    {
        _inputActions = new InputActions();
    }

    private void OnEnable()
    {
        _inputActions.Enable();
        _inputActions.Gameplay.Attack.performed += Attack;
    } 

    private void OnDisable()
    {
        _inputActions.Disable();
        _inputActions.Gameplay.Attack.performed -= Attack;
    }

    private void Update()
    {
        ReadRotateInput();
        Rotate();
    }

    private void ReadRotateInput()
    {
        _rotateInput = _inputActions.Gameplay.Rotate.ReadValue<Vector2>();
    }

    private void Rotate()
    {
        _controllableObject.Rotate(_rotateInput);
    }

    private void Attack(InputAction.CallbackContext obj)
    {
        _controllableObject.Attack();
    }
}
