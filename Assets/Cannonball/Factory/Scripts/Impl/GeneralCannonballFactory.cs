using System.Collections.Generic;
using UnityEngine;

public class GeneralCannonballFactory : CannonballFactoryBase
{
    [SerializeField] private CannonballBase _generalCannonballPrefab;

    protected override CannonballBase GetPrefab(CannonballType cannonballType)
    {
        switch (cannonballType) 
        {
            case CannonballType.GeneralCannonball:
                return _generalCannonballPrefab;
        }
        return _generalCannonballPrefab;
    }
}
