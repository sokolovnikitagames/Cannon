using System.Collections.Generic;
using UnityEngine;

public abstract class CannonballFactoryBase : MonoBehaviour
{
    public CannonballBase Spawn(CannonballType cannonballType, Vector3 position, Quaternion quaternion) 
    {
        CannonballBase cannonball = GetPrefab(cannonballType);
        return Instantiate(cannonball, position, quaternion);
    }

    public enum CannonballType
    {
        GeneralCannonball
    }

    protected abstract CannonballBase GetPrefab(CannonballType cannonballType);   
}
