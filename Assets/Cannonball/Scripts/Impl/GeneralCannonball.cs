using UnityEngine;

public class GeneralCannonball : CannonballBase
{
    protected override void Explode()
    {
        
    }

    protected override void Fly()
    {
        _cannonballRigidbody.velocity = _cannonballRigidbody.transform.forward * _flySpeed;
    }
}
