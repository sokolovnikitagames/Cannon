using System.Collections;
using UnityEngine;

public abstract class CannonballBase : MonoBehaviour
{
    [SerializeField] protected float _flySpeed;
    [SerializeField] protected float _damage;
    [SerializeField] private ParticleSystem _exploison;
    [SerializeField] private MeshRenderer _meshRenderer;

    protected Rigidbody _cannonballRigidbody;
    
    private void Awake()
    {
        _cannonballRigidbody = GetComponent<Rigidbody>();
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    public void FixedUpdate()
    {
        Fly();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Explode();
        collision.gameObject.GetComponent<IMobGetDamageable>()?.GetDamage(_damage);
        _exploison.Play();
        _meshRenderer.enabled = false;
        StartCoroutine(DestroyCannonball());
    }

    private IEnumerator DestroyCannonball()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

    protected abstract void Fly();

    protected abstract void Explode();   
}
