using UnityEngine;

public interface IRecoilable
{
    public void Recoil();
}
