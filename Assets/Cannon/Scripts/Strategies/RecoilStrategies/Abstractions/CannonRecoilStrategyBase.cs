using UnityEngine;

public abstract class CannonRecoilStrategyBase : IRecoilable
{
    protected CannonBase _cannonGameObject;
    protected Rigidbody _cannonRigidbody;

    public CannonRecoilStrategyBase(CannonBase cannonGameObject, Rigidbody cannonRigidbody) 
    {
        _cannonRigidbody = cannonRigidbody;
        _cannonGameObject = cannonGameObject;
    }

    public abstract void Recoil();
}
