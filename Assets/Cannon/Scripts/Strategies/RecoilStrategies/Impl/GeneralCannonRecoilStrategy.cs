using System.Collections;
using UnityEngine;

public class GeneralCannonRecoilStrategy : CannonRecoilStrategyBase
{
    private Vector3 _startPosition;

    public GeneralCannonRecoilStrategy(CannonBase cannonGameObject, Rigidbody cannonRigidbody) 
        : base(cannonGameObject, cannonRigidbody)
    {
    }

    public override void Recoil()
    {
        _cannonGameObject.StartCoroutine(StartRecoil());
    }

    private IEnumerator StartRecoil()
    {
        _startPosition = _cannonRigidbody.transform.position;
        _cannonRigidbody.AddForce(_cannonRigidbody.transform.right * 20, ForceMode.Impulse);
        yield return new WaitForSeconds(0.2f);
        ReturnInStartPosition();
    }

    private void ReturnInStartPosition()
    {
        _cannonRigidbody.velocity = Vector3.zero;
        _cannonGameObject.transform.position = Vector3.MoveTowards(_cannonRigidbody.transform.position, 
            _startPosition, 5f);
    }
}
