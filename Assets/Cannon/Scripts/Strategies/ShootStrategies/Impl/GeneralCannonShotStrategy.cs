using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralCannonShotStrategy : CannonShotStrategyBase
{
    public override void Shoot(CannonballFactoryBase cannonballFactory, 
        CannonballFactoryBase.CannonballType cannonballType, Vector3 shootPoint, Quaternion quaternion)
    {
        cannonballFactory.Spawn(cannonballType, shootPoint, quaternion);
    }
}
