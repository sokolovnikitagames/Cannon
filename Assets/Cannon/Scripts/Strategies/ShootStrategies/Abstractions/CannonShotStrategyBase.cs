using UnityEngine;

public abstract class CannonShotStrategyBase : IShootable
{
    public abstract void Shoot(CannonballFactoryBase cannonballFactory,
        CannonballFactoryBase.CannonballType cannonballType, Vector3 attackPoint, Quaternion quaternion);
}
