using System;
using System.Threading.Tasks;
using UnityEngine;

public class GeneralStatsStrategy : IStats
{
    private CannonStats _stats;
    
    public CannonStats Stats { get {  return _stats; } 
        private set { _stats = value; } }

    public GeneralStatsStrategy()
    {
        _stats = ScriptableObject.CreateInstance<CannonStats>();
    }

    public async void Buff(CannonStats cannonStats, float time)
    {
        Stats.ReloadSpeed += cannonStats.ReloadSpeed;
        await Task.Delay(TimeSpan.FromSeconds(time));
        Stats.ReloadSpeed -= cannonStats.ReloadSpeed;
    }

    public void Set(CannonStats stats)
    {
        _stats.ReloadSpeed = stats.ReloadSpeed;
    }
}
