using System;
using UnityEngine;

public interface IOnCannonDiedEvent
{
    public event Action OnDieEvent;
}
