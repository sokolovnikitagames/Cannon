using UnityEngine;

public interface ITransferable
{
    public void Transfer(Vector3 position);
}
