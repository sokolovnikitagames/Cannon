using UnityEngine;

public interface ICannonTransform
{
    public Transform Transform { get; }
}
