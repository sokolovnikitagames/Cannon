using UnityEngine;

public interface ICannonGetDamageable
{
    public void GetDamage(float damage);
}
