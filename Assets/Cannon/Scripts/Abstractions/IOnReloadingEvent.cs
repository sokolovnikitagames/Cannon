using System;
using UnityEngine;

public interface IOnReloadingEvent
{
    public event Action<float, float> OnReloadingEvent;
}
