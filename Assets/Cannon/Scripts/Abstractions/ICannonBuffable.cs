using UnityEngine;

public interface ICannonBuffable
{
    public void Buff(CannonStats cannonStats, float time);
}
