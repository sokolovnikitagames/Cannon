using UnityEngine;
using Cinemachine;
using System;
using System.Threading.Tasks;

public abstract class CannonBase : MonoBehaviour, ICannon
{
    [SerializeField] private float _rotateSpeed;
    [SerializeField] private float _maxHealthPoints;
    [SerializeField] private GameObject _shotPoint;
    [SerializeField] private ParticleSystem _shootExploison;

    [SerializeField] protected GameObject _cannonBody;
    [SerializeField] protected CannonballFactoryBase.CannonballType _cannonballType;
    [SerializeField] protected CannonballFactoryBase _cannonballFactory;
    [SerializeField] protected CannonStats _cannonStartStats;

    public event Action<float, float> OnChangedHealthsEvent;
    public event Action<float, float> OnReloadingEvent; 
    public event Action OnDieEvent;

    protected CinemachineImpulseSource _cinemachineImpulseSource;

    protected Rigidbody _rigidbody;
    protected IRotateable _rotateStrategy;
    protected IShootable _shootStrategy;
    protected IRecoilable _recoilStrategy;
    protected IStats _statsStrategy;
    protected float _actualHealthPoints;
    protected float _maxReload = 100;
    protected float actualReload = 100;
    protected bool _isReloading = false;
    protected bool _isDied = false;

    private void Start()
    {
        _actualHealthPoints = _maxHealthPoints;
        _cinemachineImpulseSource = GetComponent<CinemachineImpulseSource>();
        _rigidbody = GetComponent<Rigidbody>();
        InitStrategies();
        _statsStrategy.Set(_cannonStartStats);
    }

    public Transform Transform { get { return transform; } }

    public void Attack()
    {
        if (!_isReloading && !_isDied)
        {
            _shootStrategy.Shoot(_cannonballFactory, _cannonballType, _shotPoint.transform.position,
            transform.rotation);
            _shootExploison.Play();
            Recoil();
            Reload();
        }       
    }

    public void Buff(CannonStats cannonStats, float time)
    {
        _statsStrategy.Buff(cannonStats, time);
    }

    public void Rotate(Vector2 rotateDirection)
    {
        if (!_isDied)
            _rotateStrategy.Rotate(rotateDirection, _rotateSpeed);
    }

    public void Transfer(Vector3 position)
    {
        transform.position = position;
    }

    public void GetDamage(float damage)
    {
        if (_actualHealthPoints - damage <= 0)
            Die();
        else
        {
            _actualHealthPoints -= damage;
            OnChangedHealthsEvent(_actualHealthPoints, _maxHealthPoints);
        }
            
    }

    private void Recoil()
    {
        _cinemachineImpulseSource.GenerateImpulse(transform.forward * -1 * 3);
    }

    private async void Reload()
    {
        _isReloading = true;
        actualReload = 0;
        while (_isReloading)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.01));     
            if (actualReload + _statsStrategy.Stats.ReloadSpeed >= _maxReload)
            {
                actualReload = _maxReload;
                _isReloading = false;
            }
            else
                actualReload += _statsStrategy.Stats.ReloadSpeed;
            OnReloadingEvent(actualReload, _maxReload);
        }       
    }

    private void Die()
    {
        OnDieEvent?.Invoke();
        _isDied = true;
    }

    protected abstract void InitStrategies();   
}
