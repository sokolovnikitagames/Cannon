using System;
using System.Threading.Tasks;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class CannonMenu : MonoBehaviour
{
    [SerializeField] private CannonballBase _cannonball;
    [SerializeField] private ParticleSystem _particle;
    [SerializeField] private Transform _shotPoint;

    private void Start()
    {
        StartShoting();
    }

    private async void StartShoting()
    {
        while (true) 
        {
            await Task.Delay(TimeSpan.FromSeconds(2));
            _particle.Play();
            Instantiate(_cannonball, _shotPoint.position, transform.rotation);
        }        
    }
}
