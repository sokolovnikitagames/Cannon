using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ActualHealthPointsBar : MonoBehaviour
{
    private IOnCannonChangedHealths _changedHealthsEvent;

    private Image _actualHealthPointsBar;

    [Inject]
    private void Construct(IOnCannonChangedHealths changedHealthsEvent)
    {
        _changedHealthsEvent = changedHealthsEvent;
    }

    private void Start()
    {
        _actualHealthPointsBar = GetComponent<Image>();
    }

    private void OnEnable()
    {
        _changedHealthsEvent.OnChangedHealthsEvent += UpdateActualHealthPointsBar;
    }

    private void OnDisable()
    {
        _changedHealthsEvent.OnChangedHealthsEvent -= UpdateActualHealthPointsBar;
    }

    private void UpdateActualHealthPointsBar(float actualHealthPoints, float maxHealthPoints)
    {
        _actualHealthPointsBar.fillAmount = actualHealthPoints / maxHealthPoints;
    }
}
