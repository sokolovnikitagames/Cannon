using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ActualReloadBar : MonoBehaviour
{
    private IOnReloadingEvent _reloadingEvent;
    
    private Image _actualReloadBar;

    [Inject]
    private void Construct(IOnReloadingEvent reloadingEvent)
    {
        _reloadingEvent = reloadingEvent;
    }

    private void Start()
    {
        _actualReloadBar = GetComponent<Image>();
    }

    private void OnEnable()
    {
        _reloadingEvent.OnReloadingEvent += UpdateActualReloadBar;
    }

    private void OnDisable()
    {
        _reloadingEvent.OnReloadingEvent -= UpdateActualReloadBar;
    }

    private void UpdateActualReloadBar(float actualReload, float maxReload)
    {
        _actualReloadBar.fillAmount = actualReload / maxReload;
    }
}
