using UnityEngine;
using System.Collections;
using UnityEngine.UIElements;
using Zenject;

public abstract class MobBase : MonoBehaviour, IMobGetDamageable
{
    [SerializeField] protected float _moveSpeed;
    [SerializeField] protected float _healthPoints;
    [SerializeField] protected float _distanceToAttack;
    [SerializeField] protected float _damage;

    [SerializeField] protected CannonStats _buff;
    [SerializeField] protected float _buffTime;

    [SerializeField] protected MeshCollider _collider;

    protected ICannonTransform _cannonTransform;
    protected ICannonBuffable _cannonBuff;
    protected ICannonGetDamageable _cannonGetDamageable;
    protected IOnCannonDiedEvent _cannonDiedEvent;

    protected IMoveable _moveStrategy;
    protected IDieable _dieStrategy;
    protected Rigidbody _mobRigidbody; 
    protected Animator _animator;
    protected bool _isMoving;
    protected bool _isDied;
    protected bool _isWon;

    [Inject]
    private void Construct(ICannonTransform cannonTransform, ICannonBuffable cannonBuff,
        ICannonGetDamageable getDamageable, IOnCannonDiedEvent cannonDiedEvent)
    {
        _cannonGetDamageable = getDamageable;
        _cannonTransform = cannonTransform;
        _cannonDiedEvent = cannonDiedEvent;       
        _cannonBuff = cannonBuff;        
    }

    private void OnEnable()
    {
        _cannonDiedEvent.OnDieEvent += Win;
    }

    private void OnDisable()
    {
        _cannonDiedEvent.OnDieEvent -= Win;
    }

    private void Awake()
    {
        _isMoving = true;
        _isDied = false;
        _animator = GetComponent<Animator>();
        _mobRigidbody = GetComponent<Rigidbody>();
        InitStrategies();
        SetStartTriggers();
        SetAnimatorTriggers();
    }  

    private void Update()
    {
        Move();
        AttackCannon();
    }

    public void GetDamage(float damage)
    {
        if(_healthPoints - damage <= 0) 
            Die();
        else
            _healthPoints -= damage;
    }

    private void Move()
    {
        if (_isMoving && !_isWon)
            _moveStrategy.Move(_moveSpeed);
    }

    private void Die()
    {
        _dieStrategy.Die();
        SetDiedTriggers();
        SetAnimatorTriggers();
        _mobRigidbody.isKinematic = true;
        _collider.isTrigger = true;
        StartCoroutine(DestroyMob());
    }

    private void Win()
    {
        _isWon = true;
        _isMoving = false;
        SetAnimatorTriggers();
        _mobRigidbody.velocity = new Vector3();
    }

    private void AttackCannon()
    {
        if (transform.position.x - _cannonTransform.Transform.position.x < _distanceToAttack &&
            transform.position.z - _cannonTransform.Transform.position.z  < _distanceToAttack && 
            !_isDied)
        {
            _cannonGetDamageable.GetDamage(_damage);
            Die();
        }
    }

    private void SetStartTriggers()
    {
        _isMoving = true;
        _isDied = false;
    }

    private void SetDiedTriggers()
    {
        _isMoving = false;
        _isDied = true;
    }

    private void SetAnimatorTriggers()
    {
        _animator.SetBool("isDied", _isDied);
        _animator.SetBool("isMoving", _isMoving);
        _animator.SetBool("isWon", _isWon);
    }

    private IEnumerator DestroyMob()
    {
        yield return new WaitForSeconds(15f);
        Destroy(gameObject);
    }

    protected abstract void InitStrategies();   
}
