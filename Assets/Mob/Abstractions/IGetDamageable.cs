using UnityEngine;

public interface IMobGetDamageable
{
	public void GetDamage(float damage);
}
