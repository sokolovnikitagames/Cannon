using UnityEngine;

public abstract class MobDieStrategyBase : IDieable
{
    public abstract void Die();
}
