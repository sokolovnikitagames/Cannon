using UnityEngine;

public interface IDieable
{
    public void Die();
}
