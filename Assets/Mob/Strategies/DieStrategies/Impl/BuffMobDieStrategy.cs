using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffMobDieStrategy : MobDieStrategyBase
{
    private ICannonBuffable _cannonBuff;
    private CannonStats _buff;
    private float _buffTime;

    public BuffMobDieStrategy(ICannonBuffable cannonBuff, CannonStats buff, float buffTime)
         : base()
    {
        _cannonBuff = cannonBuff;
        _buff = buff;
        _buffTime = buffTime;
    }

    public override void Die()
    {
        _cannonBuff.Buff(_buff, _buffTime);
    }
}
