using UnityEngine;

public class ToPlayerMoveStrategy : MobMoveStrategyBase
{
    private Rigidbody _mobRigidbody;
    private Transform _cannonTransform;
    private float _rotationSpeed = 10;
    
    public ToPlayerMoveStrategy(Rigidbody mobRigidbody, Transform cannonTransform) 
        : base()
    {
        _mobRigidbody = mobRigidbody;
        _cannonTransform = cannonTransform;
    }

    public override void Move(float speed)
    {
        Vector3 cannonPosition = new Vector3(_cannonTransform.position.x, 0, _cannonTransform.position.z);

        Vector3 moveDirection = cannonPosition - _mobRigidbody.transform.position;
        _mobRigidbody.velocity = moveDirection.normalized * speed * Time.fixedDeltaTime;

        Vector3 rotation = Vector3.RotateTowards(_mobRigidbody.transform.forward,
            cannonPosition - _mobRigidbody.transform.position, 
            _rotationSpeed * Time.deltaTime, 0.0f);
        _mobRigidbody.transform.rotation = Quaternion.LookRotation(rotation);      
    }
}
