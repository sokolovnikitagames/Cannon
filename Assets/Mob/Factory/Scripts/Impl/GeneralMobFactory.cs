using UnityEngine;
using static CannonballFactoryBase;

public class GeneralMobFactory : MobFactoryBase
{
    [SerializeField] private MobBase _generalMobPrefab;
    [SerializeField] private MobBase _chestMobPrefab;

    protected override MobBase GetPrefab(MobType mobType)
    {
        switch (mobType)
        {
            case MobType.Slime:
                return _generalMobPrefab;
            case MobType.ChestBuff:
                return _chestMobPrefab;
        }
        return _generalMobPrefab;
    }
}
