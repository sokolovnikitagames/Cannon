using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MobBase
{
    protected override void InitStrategies()
    {
        _moveStrategy = new ToPlayerMoveStrategy(_mobRigidbody, _cannonTransform.Transform);
        _dieStrategy = new BuffMobDieStrategy(_cannonBuff, _buff, _buffTime);
    }
}
