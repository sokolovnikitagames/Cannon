using TMPro;
using UnityEngine;
using Zenject;

public class GameOverBar : MonoBehaviour
{
    private IOnCannonDiedEvent _cannonDiedEvent;

    private TMP_Text _gameOverBar;

    [Inject]
    private void Construct(IOnCannonDiedEvent cannonDiedEvent)
    {
        _cannonDiedEvent = cannonDiedEvent;
    }

    private void Start()
    {
        _gameOverBar = GetComponent<TMP_Text>();
    }

    private void OnEnable()
    {
        _cannonDiedEvent.OnDieEvent += EnableGameOverText;
    }

    private void OnDisable()
    {
        _cannonDiedEvent.OnDieEvent -= EnableGameOverText;
    }

    private void EnableGameOverText()
    {
        _gameOverBar.enabled = true;
    }
}
