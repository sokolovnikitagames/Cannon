using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour
{
    private const string GAME_SCENE = "GameScene";

    public void Play()
    {
        SceneManager.LoadScene(GAME_SCENE);
    }
}
